package com.example.android.empresas_android.Models;

import java.util.List;

/**
 * Created by Pedro Henrique on 06/08/2017.
 */

public class Portfolio {

    private int enterprises_number;
    private List<Object> enterprises;

    public int getEnterprises_number() {
        return enterprises_number;
    }

    public void setEnterprises_number(int enterprises_number) {
        this.enterprises_number = enterprises_number;
    }

    public List<Object> getEnterprises() {
        return enterprises;
    }

    public void setEnterprises(List<Object> enterprises) {
        this.enterprises = enterprises;
    }
}
