package com.example.android.empresas_android.Models;

/**
 * Created by Pedro Henrique on 06/08/2017.
 */

public class RootObject {

    private Investor investor;
    private Object enterprise;
    private boolean success;

    public Investor getInvestor() {
        return investor;
    }

    public void setInvestor(Investor investor) {
        this.investor = investor;
    }

    public Object getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(Object enterprise) {
        this.enterprise = enterprise;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
