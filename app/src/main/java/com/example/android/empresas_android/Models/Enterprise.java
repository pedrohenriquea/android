package com.example.android.empresas_android.Models;

/**
 * Created by Pedro Henrique on 06/08/2017.
 */

public class Enterprise {

    private int id;
    private String enterprise_name;
    private String description;
    private Object email_enterprise;
    private Object facebook;
    private Object twitter;
    private Object linkedin;
    private Object phone;
    private boolean own_enterprise;
    private Object photo;
    private int value;
    private int shares;
    private int share_price;
    private int own_shares;
    private String city;
    private String country;
    private EnterpriseType enterprise_type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEnterprise_name() {
        return enterprise_name;
    }

    public void setEnterprise_name(String enterprise_name) {
        this.enterprise_name = enterprise_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Object getEmail_enterprise() {
        return email_enterprise;
    }

    public void setEmail_enterprise(Object email_enterprise) {
        this.email_enterprise = email_enterprise;
    }

    public Object getFacebook() {
        return facebook;
    }

    public void setFacebook(Object facebook) {
        this.facebook = facebook;
    }

    public Object getTwitter() {
        return twitter;
    }

    public void setTwitter(Object twitter) {
        this.twitter = twitter;
    }

    public Object getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(Object linkedin) {
        this.linkedin = linkedin;
    }

    public Object getPhone() {
        return phone;
    }

    public void setPhone(Object phone) {
        this.phone = phone;
    }

    public boolean isOwn_enterprise() {
        return own_enterprise;
    }

    public void setOwn_enterprise(boolean own_enterprise) {
        this.own_enterprise = own_enterprise;
    }

    public Object getPhoto() {
        return photo;
    }

    public void setPhoto(Object photo) {
        this.photo = photo;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getShares() {
        return shares;
    }

    public void setShares(int shares) {
        this.shares = shares;
    }

    public int getShare_price() {
        return share_price;
    }

    public void setShare_price(int share_price) {
        this.share_price = share_price;
    }

    public int getOwn_shares() {
        return own_shares;
    }

    public void setOwn_shares(int own_shares) {
        this.own_shares = own_shares;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public EnterpriseType getEnterprise_type() {
        return enterprise_type;
    }

    public void setEnterprise_type(EnterpriseType enterprise_type) {
        this.enterprise_type = enterprise_type;
    }
}
